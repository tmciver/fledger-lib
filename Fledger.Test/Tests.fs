namespace Fledger.Test

open System
open Xunit
open Fledger.Domain
open Fledger.Application

module AccountTest =

    type InMemoryState() =
        member this.AccountStore: Map<Identifier, Account> =
            [("1", { Id = "1";
                     Name = "Checking";
                     Description = "Town and Country Checking"})] |> Map.ofSeq

        member this.AccountGroupStore: Map<Identifier, AccountGroup> =
            [("one", { Id = "one";
                       Name = "Test";
                       Description = "Test Account Group";
                       AccountGroups = [];
                       Accounts = []})] |> Map.ofSeq

        member this.Transactions =
            [("one", [{ Date = new DateTime(2019, 4, 19, 12, 0, 0);
                        Description = "Buy groceries";
                        Splits = [{ Description = "";
                                    AccountId = "123";
                                    TransactionType = Credit;
                                    Amount = { Amount=10.0m; Currency="USD" }};
                                  { Description = "";
                                    AccountId = "456";
                                    TransactionType = Debit;
                                    Amount = { Amount=10.0m; Currency="USD" }}]};
                      { Date = new DateTime(2019, 4, 19, 13, 0, 0);
                        Description = "Pay rent";
                        Splits = [{ Description = "";
                                    AccountId = "123";
                                    TransactionType = Credit;
                                    Amount = { Amount=500.0m; Currency="USD" }};
                                  { Description = "";
                                    AccountId = "789";
                                    TransactionType = Debit;
                                    Amount = { Amount=500.0m; Currency="USD" }}]}])] |> Map.ofSeq

        interface State with
            member this.GetAccountById id = Map.tryFind id this.AccountStore
            member this.GetAccountGroupById id = Map.tryFind id this.AccountGroupStore
            member this.GetTransactionsForAccount id =
                let transOption = Map.tryFind id this.Transactions
                if transOption.IsNone then [] else transOption.Value

    let state = new InMemoryState()

    [<Fact>]
    let ``Test account creation`` () =
        let acct = Account.create "Checking" "Town and Country"
        Assert.Equal("Checking", acct.Name)
        Assert.Equal("Town and Country", acct.Description)

    [<Fact>]
    let ``Test account balance`` () =
        let getTransactions id =
            // account 123: checking
            // account 456: groceries
            // account 789: rent
            Some([{ Date = new DateTime(2019, 4, 19, 12, 0, 0);
                    Description = "Buy groceries";
                    Splits = [{ Description = "";
                                AccountId = "123";
                                TransactionType = Credit;
                                Amount = { Amount=10.0m; Currency="USD" }};
                              { Description = "";
                                AccountId = "456";
                                TransactionType = Debit;
                                Amount = { Amount=10.0m; Currency="USD" }}]};
                  { Date = new DateTime(2019, 4, 19, 13, 0, 0);
                    Description = "Pay rent";
                    Splits = [{ Description = "";
                                AccountId = "123";
                                TransactionType = Credit;
                                Amount = { Amount=500.0m; Currency="USD" }};
                              { Description = "";
                                AccountId = "789";
                                TransactionType = Debit;
                                Amount = { Amount=500.0m; Currency="USD" }}]}])
        let checkingBalanceOption = Account.balance getTransactions "123"
        Assert.Equal(Some({ Amount = -510.0m; Currency="USD"}), checkingBalanceOption)

    [<Fact>]
    let ``Test account group creation`` () =
        let getAccountGroupById (id: Identifier) =
            Some({Id=id; Name="Test"; Description="Test Account Group"; AccountGroups=[]; Accounts=[]})
        let equalEventsIgnoreIdAndCreatedOn (e1: Event) (e2: Event) : Boolean =
            match (e1, e2) with
                | (AccountGroupCreated(cId1, _, pId1, name1, desc1, _), AccountGroupCreated(cId2, _, pId2, name2, desc2, _)) ->
                    cId1 = cId2 && pId1 = pId2 && name1 = name2 && desc1 = desc2 || failwith "Events are not equal!"
                | (_, _) -> failwith "Events not equal!"
        let commandId = Guid.NewGuid().ToString()
        let parentId = "one"
        let accountGroupId = Guid.NewGuid().ToString()
        let command = CreateAccountGroup(commandId, parentId, "An Account Group", "My new Account Group")
        let result = Application.handleCommand state command
        let expectedEvent = AccountGroupCreated(commandId, accountGroupId, parentId, "An Account Group", "My new Account Group", DateTime.Now)
        match result with
            | Ok([event']) -> equalEventsIgnoreIdAndCreatedOn event' expectedEvent
            | _ -> failwith "Events not equal."

    [<Fact>]
    let ``Test create an account`` () =
        let commandId = Guid.NewGuid().ToString()
        let accountName = "Checking"
        let accountDescription = "Town and Country Checking Account"
        let accountGroupId = Guid.NewGuid().ToString()
        let fakeAccountId = Guid.NewGuid().ToString()
        let command = CreateAccount(commandId, accountName, accountDescription, accountGroupId)
        let result = Application.handleCommand state command
        let expectedEvent = AccountCreated(commandId, fakeAccountId, "Checking", "Town and Country Checking Account", accountGroupId, DateTime.Now)
        let equalEventsIgnoreIdAndCreatedOn (e1: Event) (e2: Event) : Boolean =
            match (e1, e2) with
                | (AccountCreated(cId1, _, name1, desc1, ag1, _), AccountCreated(cId2, _, name2, desc2, ag2, _)) ->
                    cId1 = cId2 && name1 = name2 && desc1 = desc2 && ag1 = ag2 || failwith "Events are not equal!"
                | (_, _) -> failwith "Events not equal!"
        match result with
            | Ok([event']) -> equalEventsIgnoreIdAndCreatedOn event' expectedEvent
            | _ -> failwith "Events not equal."
