namespace Fledger.Application

open System
open Fledger.Domain

type Command =
    | CreateAccountGroup of id: Identifier * parentId: Identifier * name: string * description: string
    | CreateAccount of id: Identifier * name: string * description: string * accountGroupId: Identifier

type Event =
    | AccountGroupCreated of commandId: Identifier * id: Identifier * parentId: Identifier * name: string * description: string * createdOn: DateTime
    | AccountCreated of commandId: Identifier * accountId: Identifier * name: string * description: string * accountGroupId: Identifier * createdOn: DateTime

type State =
    abstract member GetAccountById: Identifier -> Account option
    abstract member GetAccountGroupById: Identifier -> AccountGroup option
    abstract member GetTransactionsForAccount: Identifier -> Transaction list

module Application =

    let handleCommand (state: State) (command: Command) : Result<Event list, string> =
        match command with
            | CreateAccountGroup(commandId, parentId, name, desc) ->
                match (state.GetAccountGroupById parentId) with
                    | Some(parent) ->
                        let accountGroupId = Guid.NewGuid().ToString()
                        Ok [AccountGroupCreated(commandId, accountGroupId, parentId, name, desc, DateTime.Now)]
                    | None -> Error "Cannot create account group; parent does not exist."
            | CreateAccount(commandId, name, desc, accountGroupId) ->
                let accountId = Guid.NewGuid().ToString()
                Ok([AccountCreated(commandId, accountId, name, desc, accountGroupId, DateTime.Now)])
