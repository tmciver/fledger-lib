namespace Fledger.Domain

open System

type Identifier = string
type Currency = string
type Money = { Amount:decimal; Currency:Currency }
with
    static member ( + ) (lhs:Money, rhs:Money) =
        if lhs.Currency <> rhs.Currency then invalidOp "Currency mismatch"
        { lhs with Amount=lhs.Amount + rhs.Amount}
    static member ( - ) (lhs:Money, rhs:Money) =
        if lhs.Currency <> rhs.Currency then invalidOp "Currency mismatch"
        { lhs with Amount=lhs.Amount - rhs.Amount}
    override money.ToString() = sprintf "%M%s" money.Amount money.Currency

type Account =
    { Id : Identifier;
      Name : string;
      Description : string }

type AccountGroup =
    { Id: Identifier;
      Name: string;
      Description: string;
      AccountGroups: AccountGroup list;
      Accounts: Account list }

type TransactionType = Debit | Credit

type Split =
    { Description: string
      AccountId : Identifier
      TransactionType: TransactionType
      Amount: Money }

type Transaction =
    { Date: DateTime;
      Description: string
      Splits: Split list }

module Account =
    let create name desc =
        let someInt = (new Random()).Next()
        let id = someInt.ToString()
        { Id = id; Name = name; Description = desc }

    // let transfer from' to' amount =
    //     { fromAccount = from'.Id; toAccount = to'.Id; amount = amount }

    let balance (getById: Identifier -> Transaction list Option) (acctId: Identifier) : Money Option =
        let transOption = getById acctId
        let transactionsToSplits transactions = seq {
            for t in transactions do yield! t.Splits
        }
        let splitsOption = Option.map transactionsToSplits transOption
        let f (balance: Money) (split: Split) : Money =
            if split.AccountId = acctId then
                //let (Money amount) = split.Amount
                match split.TransactionType with
                    | Debit -> balance + split.Amount
                    | Credit -> balance - split.Amount
            else balance
        Option.map (Seq.fold f { Amount = 0.0m; Currency = "USD"}) splitsOption
